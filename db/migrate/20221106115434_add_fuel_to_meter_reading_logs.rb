class AddFuelToMeterReadingLogs < ActiveRecord::Migration[7.0]
  def change
    add_reference :meter_reading_logs, :fuel, foreign_key: true
  end
end
