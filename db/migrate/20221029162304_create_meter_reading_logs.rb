class CreateMeterReadingLogs < ActiveRecord::Migration[7.0]
  def change
    create_table :meter_reading_logs do |t|
      t.integer :lastest_unit
      t.datetime :dated

      t.timestamps
    end
  end
end
