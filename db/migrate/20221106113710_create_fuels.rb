class CreateFuels < ActiveRecord::Migration[7.0]
  def change
    create_table :fuels do |t|
      t.decimal :price
      t.string :type_
      t.date :date

      t.timestamps
    end
  end
end
