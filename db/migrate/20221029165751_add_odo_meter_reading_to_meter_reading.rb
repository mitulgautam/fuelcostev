class AddOdoMeterReadingToMeterReading < ActiveRecord::Migration[7.0]
  def up
    add_column :meter_reading_logs, :odo_meter_reading, :integer
  end

  def down
    remove_column :meter_reading_logs, :odo_meter_reading, :integer
  end
end
