class CreateConfigurations < ActiveRecord::Migration[7.0]
  def change
    create_table :configurations do |t|
      t.decimal :fuel_price, null: false
      t.decimal :electricity_per_unit, null: false

      t.timestamps
    end
    Configuration.create!(fuel_price: 96.58, electricity_per_unit: 8.0)
  end
end
