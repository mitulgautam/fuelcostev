ActiveAdmin.register Fuel do
  permit_params :price, :type_, :date

  form do |f|
    f.semantic_errors # shows errors on :base
    inputs 'Details' do
      input :type_, as: :select, collection: Fuel.type_s
      input :price
      input :date
    end
    f.actions         # adds the 'Submit' and 'Cancel' buttons
  end
end
