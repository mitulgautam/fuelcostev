ActiveAdmin.register Configuration do
  permit_params :fuel_price, :electricity_per_unit
end
