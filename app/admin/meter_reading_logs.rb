ActiveAdmin.register MeterReadingLog do
  permit_params :lastest_unit, :dated, :odo_meter_reading, :fuel_id

  form do |f|
    f.semantic_errors # shows errors on :base
    inputs 'Details' do
      input :lastest_unit
      input :dated
      input :odo_meter_reading
      input :fuel
    end
    f.actions         # adds the 'Submit' and 'Cancel' buttons
  end
end
