class MeterReadingLog < ApplicationRecord
    belongs_to :fuel
    validates :fuel, presence: true
end
