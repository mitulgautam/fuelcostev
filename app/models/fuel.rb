class Fuel < ApplicationRecord
    enum type_: {PETROL: "PETROL", CNG: "CNG", DIESEL: "DIESEL"}
    validates :type_, presence: true
    validates :date, presence: true
    validates :price, presence: true

    attr_accessor :name
    def name
        self.date.to_s + " ₹" + self.price.to_s
    end
end
